/*
CIS 407-Winter 2015
Chat program for java
Authors: Logan Donielson, Darrekk Hocking

Todo: Make client/server chat work on seperate machines
	  Implement p2p chat
	  Implement encrypted chats
	  Implement GUI


Starting code created for:
Cis 212-Prof. Willis-Spring 2014
Assignment 8- Networking
Author: Logan Donielson
*/

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class TCPServer {
	private Socket socket = null;
	private ServerSocket server = null;
	private ObjectOutputStream objectstreamOut = null;
	private ObjectInputStream objectstreamIn = null;
	
	public TCPServer(int port) {
		try {
			System.out.println("Binding to port " + port + ", please wait  ...");
			
			server = new ServerSocket(port);
			
			System.out.println("Server started: ");
			System.out.println("Waiting for a client ...");
			
			socket = server.accept();
			System.out.println("Client accepted: " + socket);
			
			int arraysize = 0;
			int userInput = 0;
			int total = 0;
			objectstreamOut = new ObjectOutputStream(socket.getOutputStream());
			objectstreamIn = new ObjectInputStream(socket.getInputStream());
			arraysize = objectstreamIn.readInt();
				
				for(int i=0; i<arraysize; i++){
					userInput = objectstreamIn.readInt();
					total = total + userInput; 
					}
			
			objectstreamOut.writeInt(total);
			objectstreamOut.flush();
		}
		catch (IOException ioe) {
			System.out.println(ioe);
			}
		}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Server started");
		TCPServer server = new TCPServer(1030);
		}
}