/*
CIS 407-Winter 2015
Chat program for java
Authors: Logan Donielson, Darrekk Hocking

Todo: Make client/server chat work on seperate machines
	  Implement p2p chat
	  Implement encrypted chats
	  Implement GUI


Starting code created for:
Cis 212-Prof. Willis-Spring 2014
Assignment 8- Networking
Author: Logan Donielson
*/

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;

public class TCPClient {
	private Socket socket = null;
	private ObjectInputStream objectstreamIn = null;
	private ObjectOutputStream objectstreamOut = null;
	
	public TCPClient(int port) throws IOException{
		Scanner input = new Scanner(System.in);
		String value = " ";
		ArrayList<Integer> integers = new ArrayList();
		System.out.println("Establishing connection...");
		
		try{
			socket = new Socket("localhost" , 1030 );
			objectstreamIn = new ObjectInputStream(socket.getInputStream());
			objectstreamOut = new ObjectOutputStream(socket.getOutputStream());
			}
		
		catch (IOException e){
			e.printStackTrace();
			}
		
		while (!value.isEmpty()){
			
				System.out.println("Enter an integer:");
				value = input.nextLine();
				
				if (!value.isEmpty()){
					int inputInts = Integer.parseInt(value);
					integers.add(inputInts);
					}
		}
		int arraySize = integers.size();
		
		objectstreamOut.writeInt(arraySize);
		objectstreamOut.flush();
			
		for (int i = 0; i < arraySize; i++){
				objectstreamOut.writeInt(integers.get(i));
				objectstreamOut.flush();
				} 		
		
		int sum=0;
		sum = objectstreamIn.readInt();
		System.out.println("Sum = " + sum );
		}
		
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		System.out.println("Client started");
		TCPClient newClient = new TCPClient(1030);
	}
}