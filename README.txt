######################################################################################
                    ___                  _____ _           _   
                   |_  |                /  __ \ |         | |  
                     | | __ ___   ____ _| /  \/ |__   __ _| |_ 
                     | |/ _` \ \ / / _` | |   | '_ \ / _` | __|
                 /\__/ / (_| |\ V / (_| | \__/\ | | | (_| | |_ 
                 \____/ \__,_| \_/ \__,_|\____/_| |_|\__,_|\__|
######################################################################################
Current Permissions:
Logan Donielson: Admin
Darrekk Hocking: Write
Eric D. Wills: Read

Pull requests may be sent to:
logand@uoregon.edu

Abstract:
JavaChat is a secure Java-based chat program which will allow secure command-line chat between multiple machines, using UDP protocol and Javax.crypto package for text ciphers if desired.

Project Summary:
Our goal for this project is to create a simple Java-based chat program that uses a multicast client/server setup that can encrypt messages, and can be used with either the command line or a GUI.  Each team member will be responsible for different aspects of the project, but we will also use pair-programming for certain tasks. We will use a public BitBucket repository for all version control.
The program will take UDP Datagrams sent by clients to a server, and rebroadcast them to all connected clients. For now, user will need the specific IP address and port number to connect to the server.
It will also implement the javax.crypto package to encrypt messages on the client side, and decrypt on the server/peer side.
Finally, we will implement a simple text chat GUI for further ease of use.

Known issues:
UDP multicast needs to be setup.
Need to test with multiple clients/machines.
Encrypted chat (ciphers) not implemented.




Contributors: Logan Donielson, Darrekk Hocking
This project was originally started for the CIS 407 term project.

Thanks to network-science.de for the ascii generator used for the header
http://www.network-science.de/ascii/