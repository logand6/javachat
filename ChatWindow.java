import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class ChatWindow {

    JFrame firstFrame = new JFrame("Enter Information");
    JFrame chatFrame = new JFrame("Chat Client");
    JButton send;
    JTextField messageBox;
    JTextArea chatRoom;
    JTextField usernameField, hostField, portField;
    String username, host;
    int port;
    JLabel usernameLabel, hostLabel, portLabel;
    JButton startChat;

    ChatWindow(){
        this.firstWindow();
        this.username = "Anonymous";
        this.host = "localhost";
        this.port = 6789;
    }
    public void firstWindow() {
        chatFrame.setVisible(false);
        
        usernameField = new JTextField("Anonymous", 15);
        usernameLabel = new JLabel("Pick a username:");
        
        hostField = new JTextField("localhost");
        hostLabel = new JLabel("Server Address:");

        portField = new JTextField("" + 6789);
        portLabel = new JLabel("Port Number:");

        startChat = new JButton("Start Chatting");
        startChat.addActionListener(new startChatButtonListener());
        
        JPanel firstPanel = new JPanel(new GridLayout(4,3));

        firstPanel.add(usernameLabel);
        firstPanel.add(usernameField);
        firstPanel.add(hostLabel);
        firstPanel.add(hostField);
        firstPanel.add(portLabel);
        firstPanel.add(portField);
       
        firstFrame.add(BorderLayout.WEST, firstPanel);
        firstFrame.add(BorderLayout.SOUTH, startChat);

        firstFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        firstFrame.setSize(400, 200);
        firstFrame.setVisible(true);

    }

   public void chatWindow() {
        JPanel chatPanel = new JPanel();
        chatPanel.setLayout(new BorderLayout());

        JPanel southPanel = new JPanel();   
        southPanel.setBackground(Color.WHITE);
        southPanel.setLayout(new GridBagLayout());

        messageBox = new JTextField(30);
        messageBox.requestFocusInWindow();

        send = new JButton("Send");
        send.addActionListener(new sendButtonListener());

        chatRoom = new JTextArea("Welcome to the Chat Room, " + username + "!");
        chatRoom.setEditable(false);
        chatRoom.setLineWrap(true);
        chatPanel.add(new JScrollPane(chatRoom), BorderLayout.CENTER);

        GridBagConstraints left = new GridBagConstraints();
        left.anchor = GridBagConstraints.LINE_START;
        left.fill = GridBagConstraints.HORIZONTAL;
        left.weightx = 512.0;
        left.weighty = 1.0;

        GridBagConstraints right = new GridBagConstraints();
        right.insets = new Insets(0, 10, 0, 0);
        right.anchor = GridBagConstraints.LINE_END;
        right.fill = GridBagConstraints.NONE;
        right.weightx = 1.0;
        right.weighty = 1.0;

        southPanel.add(messageBox, left);
        southPanel.add(send, right);

        chatPanel.add(BorderLayout.SOUTH, southPanel);

        chatFrame.add(chatPanel);
        chatFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        chatFrame.setSize(400, 400);
        chatFrame.setVisible(true);
    }
    class startChatButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent event) {
                
            username = usernameField.getText();
            port = Integer.parseInt(portField.getText());
            host = hostField.getText();

            firstFrame.setVisible(false);
            chatWindow();
        }

    }
    class sendButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            messageBox.setText("");
        }
    }
    public static void main(String[] args) {
        new ChatWindow();
    } 
}
    


