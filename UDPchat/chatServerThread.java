import java.io.*;
import java.net.*;
import java.util.*;

public class chatServerThread extends Thread {


    public chatServerThread() throws IOException {
        super("Chat Server");
    }

    public void run() {
    	
    	InetAddress group = InetAddress.getByName("228.5.6.7");
    	MulticastSocket socket = new MulticastSocket(6789);
    	socket.joinGroup(group);

        while (socket.isConnected()) {
            try {
                byte[] buf = new byte[1000];

                DatagramPacket recv = new DatagramPacket(buf, buf.length);
                socket.receive(recv);
                String received = new String(recv.getData(), 0, recv.getLength());

                DatagramPacket send = new DatagramPacket(received.getBytes(), received.length(),
                						group, 6789);
                socket.send(send);


            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    socket.leaveGroup(group);
	socket.close();
    }
}