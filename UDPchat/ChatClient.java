import java.io.*;
import java.net.*;
import java.util.*;
import java.util.Scanner;

public class ChatClient {

    public static void main(String[] args) throws IOException {

        InetAddress group = InetAddress.getByName("228.5.6.7");
        MulticastSocket socket = new MulticastSocket(6789);
       
        socket.joinGroup(group);

        Scanner input = new Scanner(System.in);
        String msg = " ";

        //receive chat
	    byte[] buf = new byte[1000];
        DatagramPacket recv = new DatagramPacket(buf, buf.length);
        socket.receive(recv);
        String received = new String(recv.getData(), 0, recv.getLength());
        System.out.println("Received: " + received);

        //send chat
        while(!msg.isEmpty()){
            System.out.println("Enter message: ");
            msg = input.nextLine();

            DatagramPacket send = new DatagramPacket(msg.getBytes(), msg.length(),
                                        group, 6789);
                socket.send(send);
        }



	socket.leaveGroup(group);
	socket.close();
    }

}